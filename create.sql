drop table Formation;
drop table Contrats;
drop table Competence;
drop table Formateur;

create table Formateur (
    idEnseignant number(4) primary key,
    nom varchar2(32),
    prenom varchar2(32),
    dateNaiss Date
);

create table Competence(
    idEnseignant number(4),
    matiere varchar2(32),
    constraint PK_Competence primary key (idEnseignant, matiere),
    constraint FK_Competence_Formateur foreign key (idEnseignant) references Formateur (idEnseignant)
);

create table Contrats(
    idEnseignant number(4),
    societe varchar2(32),
    constraint PK_Contrats primary key (idEnseignant, societe),
    constraint FK_Contrats_Formateur foreign key (idEnseignant) references Formateur (idEnseignant)
);

create table Formation(
    societe varchar2(32),
    matiere varchar2(32),
    constraint PK_Formation primary key (societe, matiere)
);

INSERT INTO Formateur VALUES (1, 'Boichut', 'Yohan', TO_DATE('1980/12/25', 'yyyy/mm/dd'));
INSERT INTO Formateur VALUES (2, 'Exbrayat', 'Mathieu', TO_DATE('1970/11/23', 'yyyy/mm/dd'));
INSERT INTO Formateur VALUES (3, 'Moal', 'Frederic', TO_DATE('1990/07/06', 'yyyy/mm/dd'));

INSERT INTO Competence VALUES (1, 'PAJ');
INSERT INTO Competence VALUES (2, 'Maths');
INSERT INTO Competence VALUES (3, 'PAJ');

INSERT INTO Contrats VALUES (1, 'Sopra Steria');
INSERT INTO Contrats VALUES (2, 'Cap Gemini');
INSERT INTO Contrats VALUES (3, 'Atos');

INSERT INTO Formation VALUES ('Sopra Steria', 'PAJ');
INSERT INTO Formation VALUES ('Atos', 'Maths');